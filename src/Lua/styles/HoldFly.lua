-- toggle variable
local holdflymode = true

-- hold fly mode
addHook('JumpSpecial', function(player)
	if player.mo and player.mo.valid and player.mo.skin != eggman
		-- check the situation is valid
		if (holdflymode) and (player.cmd.buttons & BT_JUMP) and (player.powers[pw_tailsfly] > 0)
			-- tell the game to propel the player into the air in flight state.
			player.fly1 = 1
		end
	end
end)

local enableHoldFly = function(player, arg1)
	-- toggle
	if arg1 == nil then
		holdflymode = not holdflymode
		local onoffprint = "off"
		if (holdflymode) then onoffprint = "on" end
		CONS_Printf(player, "hold fly "..onoffprint..".")
		return
	end

	-- turn on
	if arg1:lower() == "on" or arg1:lower() == "yes" or arg1 == "1" or arg1:lower() == "true" then
		holdflymode = true
		CONS_Printf(player, "hold fly on.")
		return
	end
	
	-- turn off
	if arg1:lower() == "off" or arg1:lower() == "no" or arg1 == "0" or arg1:lower() == "false" then
		holdflymode = false
		CONS_Printf(player, "hold fly off.")
		return
	end

	-- invalid
	CONS_Printf(player, "Invalid argument specified. use 'holdfly' to toggle, or use on/yes/1/true or off/no/0/false.")
	return
end

-- hook up command
COM_AddCommand("holdfly", enableHoldFly, 0)