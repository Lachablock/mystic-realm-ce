--coded by Sylve, SimplerHud code by SteelT referenced to figure out how to draw the life icons and such in the right places, mainly how to use v.getSprite2Patch (thank you because I was really confused on how to do hud code lol)

freeslot(
"SKINCOLOR_COMPLETEBLACKDROPSHADOW" --name is very long and very specific to avoid conflicts
)

skincolors[SKINCOLOR_COMPLETEBLACKDROPSHADOW] = {
	name = "CompleteBlack",
	ramp = {31,31,31,31,31,31,31,31,31,31,31,31,31,31,31,31},
	invcolor = SKINCOLOR_WHITE,
	invshade = 1,
	chatcolor = V_GRAYMAP,
	accessible = false
}

local function DrawLivesIcons(v, p, cam)
local skipcheck
if (p.mo.skin == "skip") then
	skipcheck = -30 --scrap
else
	skipcheck = 0
end

	if (p.realmo) and not (p.hypermysticsonic) and not (p.mo.skin == "modernsonic") and not (srb2p) and not (maptol & TOL_NIGHTS) and not (G_IsSpecialStage(gamemap)) then
		local contsprite = v.getSprite2Patch(p.realmo.skin, SPR2_XTRA, false, C) or v.getSprite2Patch(p.realmo.skin, SPR2_XTRA, false, A) or v.getSprite2Patch(p.realmo.skin, SPR2_STND, false, A, 2) --last resort is stnd, please do not do this it looks BAD
		
		v.draw(hudinfo[HUD_LIVES].x, hudinfo[HUD_LIVES].y-16+skipcheck,  v.cachePatch("MRLIVEBK"), V_SNAPTOBOTTOM|V_SNAPTOLEFT|V_PERPLAYER, v.getColormap(p.realmo.skin, p.realmo.color))
		
		if not (p.spectator) then
			v.drawScaled((hudinfo[HUD_LIVES].x+18)*FRACUNIT, (hudinfo[HUD_LIVES].y+10+skipcheck)*FRACUNIT, FRACUNIT, contsprite, V_SNAPTOBOTTOM|V_PERPLAYER|V_SNAPTOLEFT|((p.spectator) and V_HUDTRANSHALF or V_HUDTRANS), v.getColormap(TC_BLINK, SKINCOLOR_COMPLETEBLACKDROPSHADOW)) --dropshadow goes first
			v.drawScaled((hudinfo[HUD_LIVES].x+17)*FRACUNIT, (hudinfo[HUD_LIVES].y+8+skipcheck)*FRACUNIT, FRACUNIT, contsprite, V_SNAPTOBOTTOM|V_PERPLAYER|V_SNAPTOLEFT|V_HUDTRANS, v.getColormap(p.realmo.skin, p.realmo.color)) --then the actual player sprite
		else
			v.drawScaled((hudinfo[HUD_LIVES].x+17)*FRACUNIT, (hudinfo[HUD_LIVES].y+3)*FRACUNIT, FRACUNIT, contsprite, V_SNAPTOBOTTOM|V_PERPLAYER|V_SNAPTOLEFT|((p.spectator) and V_HUDTRANSHALF), v.getColormap(TC_RAINBOW, p.realmo.color)) --there is no dropshadow when you're spectating; you're a ghost
		end
		
			
		
		if (hud.enabled("lives")) then 
			hud.disable("lives")
		end
		
		if (customhud) then --not you either custom hud
			customhud.disable("lives")
		end
		
		if (G_GametypeUsesLives()) then
				if not (p.lives == INFLIVES) and not (cv_cooplives == "Infinite") then --there is nothing of the sort in gamemodes without lives
					v.drawNum(hudinfo[HUD_LIVES].x+68, hudinfo[HUD_LIVES].y+5+skipcheck, p.lives, hudinfo[HUD_LIVES].f|V_PERPLAYER|V_HUDTRANS|V_SNAPTOBOTTOM)
				end
		end
		
	end
end

/*

Having this be part of the player thinker means it doesnt work when the game is in the Esc menu
I'm not concerned with the OLDC or srb2p for this mod, so...

addHook("PlayerThink", function(p)
	if (p.mo.skin != "modernsonic") and not (srb2p) then
		if (hud.enabled("lives")) then --I don't want that stuff in my HOUSE (this is done in a playerthink because I need to make sure it doesn't happen again in like, oldc or uldc)
			hud.disable("lives")
		end
		if (customhud) then --not you either custom hud
			customhud.disable("lives")
		end
	end
end)
*/

hud.add(DrawLivesIcons, "game")

